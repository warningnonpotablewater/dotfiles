function rsync --wraps=rsync
    command rsync\
        --recursive\
        --partial\
        --times\
        --progress\
        --human-readable\
        --verbose\
        $argv;
end
