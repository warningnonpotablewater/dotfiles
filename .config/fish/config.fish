set fish_private_mode maybe

set fish_color_autosuggestion brblack

if status is-login
    set env_generator /usr/lib/systemd/user-environment-generators/30-systemd-environment-d-generator

    if test -e $env_generator
        for var in ($env_generator)
            export $var
        end
    end
end
