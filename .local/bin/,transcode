#!/usr/bin/fish

function transcode
    set old_extension .$argv[1]
    set new_extension .$argv[2]
    set command $argv[3]
    set files $argv[4..]

    for input in $files
        set output (string replace $old_extension $new_extension $input)

        eval $command
        or exit 1

        if test $old_extension != $new_extension
            and rm $input
        end
    end
end

set args $argv[2..]

switch $argv[1]
    case opus
        transcode\
            flac opus\
            'ffmpeg -i $input -b:a 128k $output'\
            $args
    case jxl
        transcode\
            jpg jxl\
            'cjxl --lossless_jpeg=1 $input $output'\
            $args
    case jpegli
        # cjpegli doesn't copy metadata, so you have to do it manually.

        transcode\
            png jpg\
            '
            cjpegli\
                --quality=95\
                --chroma_subsampling=420\
                --progressive_level=2\
                $input $output

            and exiv2 -ea- $input | exiv2 -ia- $output
            '\
            $args
    case progressive-jpeg
        transcode\
            jpg jpg\
            '
            set temp (mktemp)

            and jpegtran -copy all -optimize -progressive $input > $temp

            and cat $temp > $input
            and rm $temp
            '\
            $args
    case '*'
        echo 'usage: ,transcode {opus,jxl} [file ...]'
end
